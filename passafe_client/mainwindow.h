﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCryptographicHash>
#include <QMessageBox>
#include <../passafe_database/passafe_database.h>
#include <../network/network.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    SSL_Client * m_client;
    QList<tbl_storage_entry> m_storage;
    QString m_user;
    bool is_authorized;
private slots:
    void on_connect();
    void on_disconnect();
    void on_auth();
    void on_register();
    void handle_package();
    void sync_storage(const QByteArray& ba);
    void on_edit_entry();
    void on_new_entry();
    void on_del_entry();
    void on_logout();
    void connection_success();
    void auth_reg_success();
};
#endif // MAINWINDOW_H
