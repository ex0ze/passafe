#include "register_dialog.h"
#include "ui_register_dialog.h"

register_dialog::register_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::register_dialog)
{
    ui->setupUi(this);
}

QString register_dialog::get_login() const
{
    return ui->login->text();
}

QString register_dialog::get_pass1() const
{
    return ui->pass->text();
}

QString register_dialog::get_pass2() const
{
    return ui->pass_2->text();
}

register_dialog::~register_dialog()
{
    delete ui;
}
