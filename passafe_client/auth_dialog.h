#ifndef AUTH_DIALOG_H
#define AUTH_DIALOG_H

#include <QDialog>

namespace Ui {
class auth_dialog;
}

class auth_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit auth_dialog(QWidget *parent = nullptr);
    QString get_login() const;
    QString get_password() const;
    ~auth_dialog();

private:
    Ui::auth_dialog *ui;
};

#endif // AUTH_DIALOG_H
