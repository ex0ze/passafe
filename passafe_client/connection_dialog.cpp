#include "connection_dialog.h"
#include "ui_connection_dialog.h"

connection_dialog::connection_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::connection_dialog)
{
    ui->setupUi(this);
}

QString connection_dialog::get_addr() const
{
    return ui->lineEdit->text();
}

int connection_dialog::get_port() const
{
    return ui->spinBox->value();
}

connection_dialog::~connection_dialog()
{
    delete ui;
}
