#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "connection_dialog.h"
#include "auth_dialog.h"
#include "register_dialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_client(new SSL_Client(this))
    , is_authorized(false)
{
    ui->setupUi(this);
    connect(m_client, &SSL_Client::packageReceived, this, &MainWindow::handle_package);
    connect(ui->action_connect, &QAction::triggered, this, &MainWindow::on_connect);
    connect(ui->action_auth, &QAction::triggered, this, &MainWindow::on_auth);
    connect(ui->action_register, &QAction::triggered, this, &MainWindow::on_register);
    connect(ui->btn_newEntry, &QPushButton::clicked, this, &MainWindow::on_new_entry);
    connect(ui->btn_delEntry, &QPushButton::clicked, this, &MainWindow::on_del_entry);
    connect(ui->action_disconnect, &QAction::triggered, this, &MainWindow::on_disconnect);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connect()
{
    connection_dialog dlg;
    dlg.exec();
    auto addr = dlg.get_addr();
    auto port = dlg.get_port();
    m_client->conn(addr, port);
}

void MainWindow::on_disconnect()
{
    ui->action_connect->setEnabled(true);
    ui->action_disconnect->setEnabled(false);
}

void MainWindow::on_auth()
{
    auth_dialog dlg;
    if (QDialog::Rejected == dlg.exec()) return;
    QString login = dlg.get_login();
    QString password = dlg.get_password();
    QCryptographicHash hasher(QCryptographicHash::Algorithm::Sha256);
    hasher.addData(password.toUtf8());
    password = hasher.result();
    UserAuth ua;
    ua.m_login = login;
    m_user = login;
    ua.m_password = password;
    m_client->sendPackage(NetworkPackage(NetworkPackage::Context::USER_AUTH, ua.to_raw()));
}

void MainWindow::on_register()
{
    register_dialog dlg;
    if (QDialog::Rejected == dlg.exec()) return;
    QString login = dlg.get_login();
    QString pass1 = dlg.get_pass1();
    QString pass2 = dlg.get_pass2();
    if (pass1 != pass2)
    {
        QMessageBox::critical(this, "Ошибка ввода паролей", "Пароли не совпадают");
        return;
    }
    UserAuth ua;
    QCryptographicHash hasher(QCryptographicHash::Algorithm::Sha256);
    hasher.addData(pass1.toUtf8());
    pass1 = hasher.result();
    ua.m_login = login;
    ua.m_password = pass1;
    m_user = login;
    m_client->sendPackage(NetworkPackage(NetworkPackage::Context::USER_REGISTER, ua.to_raw()));
}

void MainWindow::handle_package()
{
    auto pkg = m_client->getPackage();
    switch (pkg.m_context) {
    case NetworkPackage::Context::STATUS_HELLO: {
        connection_success();
        break;
    }
    case NetworkPackage::Context::STATUS_AUTH_OK: {
        QMessageBox::information(this, "Успешно", "Успешный вход в систему");
        is_authorized = true;
        auth_reg_success();
        m_client->sendPackage(NetworkPackage(NetworkPackage::Context::DB_SYNC_REQUEST, m_user.toUtf8()));
        break;
    }
    case NetworkPackage::Context::STATUS_REGISTER_OK:{
        QMessageBox::information(this, "Успешно", "Успешная регистрация");
        is_authorized = true;
        auth_reg_success();
        m_client->sendPackage(NetworkPackage(NetworkPackage::Context::DB_SYNC_REQUEST, m_user.toUtf8()));
        break;
    }
    case NetworkPackage::Context::STATUS_REGISTER_FAILED:{
        QMessageBox::information(this, "Ошибка", "Ошибка регистрации");
        is_authorized = false;
        break;
    }
    case NetworkPackage::Context::STATUS_AUTH_FAILED:{
        QMessageBox::critical(this, "Ошибка", "Неверный логин или пароль");
        is_authorized = false;
        break;
    }
    case NetworkPackage::Context::STATUS_AUTH_DOES_NOT_EXIST:{
        QMessageBox::critical(this, "Ошибка", "Пользователь не существует");
        is_authorized = false;
        break;
    }
    case NetworkPackage::Context::STATUS_SYNC_OK:{
        sync_storage(pkg.m_data);
        break;
    }
    }

}

void MainWindow::sync_storage(const QByteArray &ba)
{
    auto entrylist = tbl_storage_entry::raw_to_entrylist(ba);
    m_storage = entrylist;
    ui->table_entry->clear();
    ui->table_entry->setColumnCount(4);
    ui->table_entry->setHorizontalHeaderLabels({"Логин","Пароль","Описание",""});
    for (const auto & e : entrylist)
    {
        int rowCount = ui->table_entry->rowCount();
        ui->table_entry->insertRow(rowCount);
        ui->table_entry->setItem(rowCount, 0, new QTableWidgetItem(e.login));
        QTableWidgetItem * item_ptr = ui->table_entry->item(rowCount, 0);
        item_ptr->setFlags(item_ptr->flags() ^ Qt::ItemIsEditable);
        ui->table_entry->setItem(rowCount, 1, new QTableWidgetItem(e.password));
        item_ptr = ui->table_entry->item(rowCount, 1);
        item_ptr->setFlags(item_ptr->flags() ^ Qt::ItemIsEditable);
        ui->table_entry->setItem(rowCount, 2, new QTableWidgetItem(e.description));
        item_ptr = ui->table_entry->item(rowCount, 2);
        item_ptr->setFlags(item_ptr->flags() ^ Qt::ItemIsEditable);
        QPushButton * btn = new QPushButton();
        btn->setProperty("row", rowCount);
        connect(btn, &QPushButton::clicked, this, &MainWindow::on_edit_entry);
        btn->setText("Редактировать");
        ui->table_entry->setCellWidget(rowCount, 3, btn);
    }
}

void MainWindow::on_edit_entry()
{
    QPushButton * btn = qobject_cast<QPushButton*>(sender());
    QVariant row_variant = btn->property("row");
    if (!row_variant.isValid()) return;
    int row = row_variant.toInt();
    if (btn->text() == "Редактировать")
    {
        for (int i = 0; i < 3; ++i)
        {
            QTableWidgetItem * item_ptr = ui->table_entry->item(row, i);
            item_ptr->setFlags(item_ptr->flags() ^ Qt::ItemIsEditable);
        }
        btn->setText("Сохранить");
    }
    else if (btn->text() == "Сохранить")
    {
        QString text[3];
        for (int i = 0; i < 3; ++i)
        {
            QTableWidgetItem * item_ptr = ui->table_entry->item(row, i);
            text[i] = item_ptr->text();
            item_ptr->setFlags(item_ptr->flags() ^ Qt::ItemIsEditable);
        }
        tbl_storage_entry new_entry;
        new_entry.login = text[0];
        new_entry.password = text[1];
        new_entry.description = text[2];
        if (m_storage.size() == row)
        {
            m_client->sendPackage(NetworkPackage(NetworkPackage::Context::DB_ENTRY_ADD,
                                                 EntryPackage(EntryPackage::ADD, m_user, {new_entry}).to_raw()));
            m_storage << new_entry;
        }
        else {
            auto old_entry = m_storage[row];

            m_client->sendPackage(NetworkPackage(NetworkPackage::Context::DB_ENTRY_EDIT,
                                                 EntryPackage(EntryPackage::EDIT, m_user, {new_entry, old_entry}).to_raw()));
            m_storage[row] = new_entry;
        }

        btn->setText("Редактировать");
    }
}

void MainWindow::on_new_entry()
{
    int rowCount = ui->table_entry->rowCount();
    ui->table_entry->insertRow(rowCount);
    ui->table_entry->setItem(rowCount, 0, new QTableWidgetItem(""));
    ui->table_entry->setItem(rowCount, 1, new QTableWidgetItem(""));
    ui->table_entry->setItem(rowCount, 2, new QTableWidgetItem(""));
    QPushButton * btn = new QPushButton();
    btn->setProperty("row", rowCount);
    connect(btn, &QPushButton::clicked, this, &MainWindow::on_edit_entry);
    btn->setText("Сохранить");
    ui->table_entry->setCellWidget(rowCount, 3, btn);
}

void MainWindow::on_del_entry()
{
    if (!is_authorized) return;
    int row = ui->table_entry->currentRow();
    if (row < 0) return;
    ui->table_entry->removeRow(row);
    auto del_entry = m_storage[row];
    m_storage.removeAll(del_entry);
    m_client->sendPackage(NetworkPackage(NetworkPackage::Context::DB_ENTRY_DELETE,
                                         EntryPackage(EntryPackage::REMOVE, m_user, {del_entry}).to_raw()));
}

void MainWindow::on_logout()
{
    m_user = QString();
    is_authorized = false;
    ui->table_entry->setRowCount(0);
    ui->action_logout->setEnabled(false);
    ui->btn_delEntry->setEnabled(false);
    ui->btn_newEntry->setEnabled(false);
    ui->action_auth->setEnabled(true);
    ui->action_register->setEnabled(true);
}

void MainWindow::connection_success()
{
    ui->action_connect->setEnabled(false);
    ui->action_auth->setEnabled(true);
    ui->action_register->setEnabled(true);
    ui->action_disconnect->setEnabled(true);
}

void MainWindow::auth_reg_success()
{
    ui->action_auth->setEnabled(false);
    ui->action_register->setEnabled(false);
    ui->action_logout->setEnabled(true);
    ui->btn_delEntry->setEnabled(true);
    ui->btn_newEntry->setEnabled(true);
}

