#ifndef REGISTER_DIALOG_H
#define REGISTER_DIALOG_H

#include <QDialog>

namespace Ui {
class register_dialog;
}

class register_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit register_dialog(QWidget *parent = nullptr);
    QString get_login() const;
    QString get_pass1() const;
    QString get_pass2() const;
    ~register_dialog();

private:
    Ui::register_dialog *ui;
};

#endif // REGISTER_DIALOG_H
