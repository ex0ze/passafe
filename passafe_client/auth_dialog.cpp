#include "auth_dialog.h"
#include "ui_auth_dialog.h"

auth_dialog::auth_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::auth_dialog)
{
    ui->setupUi(this);
}

QString auth_dialog::get_login() const
{
    return ui->login->text();
}

QString auth_dialog::get_password() const
{
    return ui->password->text();
}

auth_dialog::~auth_dialog()
{
    delete ui;
}
