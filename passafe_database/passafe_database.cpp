#include "passafe_database.h"

Passafe_database::Passafe_database()
{
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName("passafe_database.db");
    if (!m_db.open())
    {
        qDebug() << m_db.lastError();
        return;
    }
    QSqlQuery create_users_query(createUsersQ, m_db);
    fetch_users();
    fetch_storages();
}

void Passafe_database::fetch_users()
{
    QSqlQuery select_users_query(selectUsersQ, m_db);
    qDebug() << select_users_query.executedQuery();
    m_users.clear();
    while (select_users_query.next())
    {
        m_users[select_users_query.value(0).toString()] =  select_users_query.value(1).toString();
    }
}

void Passafe_database::fetch_storage(const QString &user)
{
    QString q = QString(selectStorageQ).arg(user);
    QSqlQuery select_storage_query(q, m_db);
    qDebug() << select_storage_query.executedQuery();
    QList<tbl_storage_entry> user_storage;
    while (select_storage_query.next())
    {
        user_storage.push_back({select_storage_query.value(0).toString(),
                               select_storage_query.value(1).toString(),
                               select_storage_query.value(2).toString()});
    }
    m_storages[user]  = user_storage;
}

Passafe_database::Passafe_auth_status Passafe_database::check_auth(const QString &login, const QString &password)
{
    if (m_users.contains(login))
    {
        if (m_users[login] == password) return STATUS_OK;
        else return STATUS_WRONG_PASS;
    }
    else return STATUS_USER_DOES_NOT_EXIST;
}

bool Passafe_database::process(const EntryPackage &pkg)
{
    switch (pkg.m_context) {
    case EntryPackage::ADD:
        add_storage_entry(pkg.m_user, pkg.m_storages[0]);
        return true;
    case EntryPackage::EDIT:
        edit_storage_entry(pkg.m_user, pkg.m_storages[0], pkg.m_storages[1]);
        return true;
    case EntryPackage::REMOVE:
        remove_storage_entry(pkg.m_user, pkg.m_storages[0]);
        return true;
    default:
        return false;
    }
}

bool Passafe_database::register_user(const QString &user, const QString &password)
{
    if (m_users.contains(user)) return false;
    m_users[user] = password;
    QSqlQuery insert_user_q(QString(insertUsersQ).arg(user).arg(password), m_db);
    qDebug() << insert_user_q.executedQuery();
    QSqlQuery create_storage_q(QString(createStorageQ).arg(user), m_db);
    qDebug() << create_storage_q.executedQuery();
    return true;
}

void Passafe_database::add_storage_entry(const QString &user, const tbl_storage_entry &e)
{
    m_storages[user].push_back(e);
    QSqlQuery q(QString(insertStorageQ).arg(user).arg(e.login).arg(e.password).arg(e.description), m_db);
    qDebug() << q.executedQuery();
}

void Passafe_database::remove_storage_entry(const QString &user, const tbl_storage_entry &e)
{
    m_storages[user].removeAll(e);
    QSqlQuery q(QString(deleteStorageQ).arg(user).arg(e.login).arg(e.password), m_db);
    qDebug() << q.executedQuery();
}

void Passafe_database::edit_storage_entry(QString user, tbl_storage_entry e_new, tbl_storage_entry e_old)
{
    if (!m_storages[user].contains(e_old)) return;
    for (auto & i : m_storages[user])
    {
        if (i == e_old)
        {
            i = e_new;
            break;
        }
    }
    QSqlQuery q(QString(updateStorageQ).arg(user).arg(e_new.login).arg(e_new.password).arg(e_new.description).arg(e_old.login).arg(e_old.password), m_db);
    qDebug() << q.executedQuery();
}
void Passafe_database::fetch_storages()
{
    for (auto & u : m_users.keys())
    {
        fetch_storage(u);
    }
}

QList<tbl_storage_entry> Passafe_database::get_user_storage(const QString &user) const
{
    return m_storages.contains(user) ? m_storages[user] : QList<tbl_storage_entry>();
}
