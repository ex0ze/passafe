#ifndef PASSAFE_DATABASE_H
#define PASSAFE_DATABASE_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlError>
#include <QDebug>
#include <QHash>
#include <QString>

static const char createStorageQ[] = "CREATE TABLE \"user_%1_storage\" (\"login\" TEXT,\"password\" TEXT,\"description\" TEXT)";
static const char selectStorageQ[] = "SELECT * FROM user_%1_storage";
static const char insertStorageQ[] = "INSERT INTO user_%1_storage VALUES ('%2','%3','%4')";
static const char updateStorageQ[] = "UPDATE user_%1_storage SET login='%2', password='%3', description='%4' WHERE login='%5' AND password='%6'";
static const char deleteStorageQ[] = "DELETE FROM user_%1_storage WHERE login='%2' AND password='%3'";


static const char createUsersQ[] = "CREATE TABLE IF NOT EXISTS \"users\" (\"login\" TEXT NOT NULL UNIQUE, \"password\" TEXT NOT NULL,PRIMARY KEY(\"login\"))";
static const char selectUsersQ[] = "SELECT * FROM users";
static const char insertUsersQ[] = "INSERT INTO users VALUES('%1','%2')";

struct UserAuth
{
    QString m_login;
    QString m_password;
    QByteArray to_raw() const
    {
        QByteArray res;
        res.append(m_login);
        res.append('\n');
        res.append(m_password);
        return res;
    }
    static UserAuth from_raw(const QByteArray& ba)
    {
        UserAuth res;
        auto list = ba.split('\n');
        res.m_login = list[0];
        res.m_password = list[1];
        return res;
    }
};

struct tbl_storage_entry
{
    QString login;
    QString password;
    QString description;
    bool operator==(tbl_storage_entry rhs) const
    {
        return login == rhs.login && password == rhs.password && description == rhs.description;
    }
    QByteArray to_raw() const
    {
        QByteArray res;
        res.append(login);
        res.append('\n');
        res.append(password);
        res.append('\n');
        res.append(description);
        res.append("--new-entry--");
        return res;
    }
    static tbl_storage_entry from_raw(const QByteArray& ba, bool * conv_ok = nullptr)
    {
        tbl_storage_entry entry;
        auto list = ba.split('\n');
        if (list.size() < 3)
        {
            if (conv_ok) *conv_ok = false;
            return entry;
        }
        entry.login = list[0];
        entry.password = list[1];
        entry.description = list[2];
        if (conv_ok) *conv_ok = true;
        return entry;
    }
    static QByteArray entrylist_to_raw(const QList<tbl_storage_entry>& list)
    {
        QByteArray res;
        for (const auto & e : list) res.append(e.to_raw());
        return res;
    }
    static QList<tbl_storage_entry> raw_to_entrylist(const QByteArray& ba)
    {
        QString str = ba;
        auto list = str.split("--new-entry--");
        QList<tbl_storage_entry> entrylist;
        for (const auto & e : list)
        {
            bool conv_ok = true;
            tbl_storage_entry entry;
            if (!e.isEmpty()) {
                entry = tbl_storage_entry::from_raw(e.toUtf8(), &conv_ok);
                if (conv_ok) entrylist << entry;
            }
        }
        return entrylist;
    }
};

struct EntryPackage
{
    enum Context
    {
        UNKNOWN = 0,
        ADD     = 1,
        REMOVE  = 2,
        EDIT    = 3
    };
    Context m_context;
    QString m_user;
    QList<tbl_storage_entry> m_storages;
    EntryPackage(Context c, QString user, QList<tbl_storage_entry> storages) : m_context(c), m_user(user), m_storages(storages) {}
    QByteArray to_raw() const
    {
        QByteArray res;
        switch (m_context) {
        case ADD:
            res.append("ADD\n");
            break;
        case REMOVE:
            res.append("REMOVE\n");
            break;
        case EDIT:
            res.append("EDIT\n");
            break;
        case UNKNOWN: default: return QByteArray();
        }
        res.append(m_user);
        res.append('\n');
        auto raw_entrylist = tbl_storage_entry::entrylist_to_raw(m_storages);
        res.append(raw_entrylist);
        return res;
    }
    static EntryPackage from_raw(const QByteArray& ba, QString * pkg_user = nullptr)
    {
        Context c;
        auto l = ba.split('\n');
        if (l.isEmpty()) return EntryPackage(UNKNOWN, QString(), QList<tbl_storage_entry>());
        if (l[0] == "ADD") c = ADD;
        else if (l[0] == "REMOVE") c = REMOVE;
        else if (l[0] == "EDIT") c = EDIT;
        else return EntryPackage(UNKNOWN, QString(), QList<tbl_storage_entry>());
        QString user = l[1];
        if (pkg_user) *pkg_user = user;
        QByteArray e_list = ba.mid(l[0].size() + l[1].size() + 2);
        QList<tbl_storage_entry> storage_list = tbl_storage_entry::raw_to_entrylist(e_list);
        return EntryPackage(c, user, storage_list);
    }
};

class Passafe_database
{
public:
    enum Passafe_auth_status
    {
        STATUS_OK,
        STATUS_WRONG_PASS,
        STATUS_USER_DOES_NOT_EXIST
    };
    Passafe_database();
    void fetch_users();
    void fetch_storage(const QString& user);
    Passafe_auth_status check_auth(const QString& login, const QString& password);
    bool process(const EntryPackage& pkg);
    bool register_user(const QString& user, const QString& password);
    void add_storage_entry(const QString& user, const tbl_storage_entry& e);
    void remove_storage_entry(const QString& user, const tbl_storage_entry& e);
    void edit_storage_entry(QString user, tbl_storage_entry e_new, tbl_storage_entry e_old);
    void fetch_storages();
    QList<tbl_storage_entry> get_user_storage(const QString& user) const;
private:
    QSqlDatabase m_db;
    QHash<QString, QList<tbl_storage_entry>> m_storages;
    QMap<QString, QString> m_users;
};

#endif // PASSAFE_DATABASE_H
