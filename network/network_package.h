#ifndef NETWORK_PACKAGE_H
#define NETWORK_PACKAGE_H

#include <QByteArray>

struct NetworkPackage
{
    enum Context
    {
        UNKNOWN                     = 0,
        USER_AUTH                   = 1,
        USER_REGISTER               = 2,

        DB_SYNC_REQUEST             = 50,
        DB_ENTRY_ADD                = 51,
        DB_ENTRY_EDIT               = 52,
        DB_ENTRY_DELETE             = 53,


        STATUS_PACKAGE_OK           = 100,
        STATUS_PACKAGE_ERROR        = 101,
        STATUS_PACKAGE_WRONG_CSUM   = 102,
        STATUS_AUTH_OK              = 103,
        STATUS_AUTH_DOES_NOT_EXIST  = 104,
        STATUS_AUTH_FAILED          = 105,
        STATUS_REGISTER_OK          = 106,
        STATUS_REGISTER_FAILED      = 107,
        STATUS_ENTRY_OK             = 108,
        STATUS_ENTRY_FAILED         = 109,
        STATUS_SYNC_OK              = 110,
        STATUS_ACCESS_DENIED        = 111,
        STATUS_HELLO                = 112
    };
    Context m_context;
    quint8 mutable m_csum;
    QByteArray m_data;
    NetworkPackage(Context _context, const QByteArray& data = QByteArray(), quint8 csum = 0) :
        m_context(_context), m_data(data), m_csum(csum) {}
    bool operator==(const NetworkPackage& rhs) const
    {
        return m_context == rhs.m_context && m_data == rhs.m_data;
    }
    static quint8 get_csum(const QByteArray& ba)
    {
        quint8 csum = 0;
        for (auto const * it = ba.constData(); it != ba.constData() + ba.size(); ++it)
        {
            csum = (csum + *it) % 256;
        }
        return csum;
    }
    QByteArray to_raw() const
    {
        QByteArray res;
        res.resize(sizeof m_context + sizeof m_csum + m_data.size());
        m_csum = get_csum(m_data);
        memcpy(res.data(), &m_context, sizeof m_context);
        memcpy(res.data() + sizeof m_context, &m_csum, sizeof m_csum);
        memcpy(res.data() + sizeof m_context + sizeof m_csum, m_data.constData(), m_data.size());
        return res;
    }
    static NetworkPackage from_raw(const QByteArray& ba, bool * valid_csum = nullptr)
    {
        Context _context;
        quint8 _csum;
        QByteArray _data;
        _data.resize(ba.size() - sizeof _context - sizeof _csum);
        memcpy(&_context, ba.constData(), sizeof _context);
        memcpy(&_csum, ba.constData() + sizeof _context, sizeof _csum);
        memcpy(_data.data(), ba.constData() + sizeof _context + sizeof _csum, _data.size());
        quint8 calculated_csum = get_csum(_data);
        if (valid_csum) *valid_csum = (calculated_csum == _csum);
        return NetworkPackage(_context, _data, _csum);
    }
};


#endif // NETWORK_PACKAGE_H
