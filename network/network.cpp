#include "network.h"
#include <QDirIterator>

SSL_Server::SSL_Server(quint16 port, QObject *parent) :QObject(parent)
{
    m_serversocket = new QWebSocketServer("Passafe Server",
                                          QWebSocketServer::SecureMode,
                                          this);
    QSslConfiguration sslConfiguration;
    QFile certFile("localhost.cert");
    QFile keyFile("localhost.key");
    certFile.open(QIODevice::ReadOnly);
    keyFile.open(QIODevice::ReadOnly);
    qDebug() << "Listening port " << port;
    QSslCertificate certificate(&certFile, QSsl::Pem);
    QSslKey sslKey(&keyFile, QSsl::Rsa, QSsl::Pem);
    certFile.close();
    keyFile.close();
    sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);
    sslConfiguration.setLocalCertificate(certificate);
    sslConfiguration.setPrivateKey(sslKey);
    sslConfiguration.setProtocol(QSsl::TlsV1SslV3);
    m_serversocket->setSslConfiguration(sslConfiguration);

    if (m_serversocket->listen(QHostAddress::Any, port))
    {
        connect(m_serversocket, &QWebSocketServer::newConnection,
                this, &SSL_Server::onNewConnection);
        connect(m_serversocket, &QWebSocketServer::sslErrors,
                this, &SSL_Server::onSslErrors);
    }
}

bool SSL_Server::replyToPackage(const NetworkPackage &to, const NetworkPackage &with)
{
    NetworkAddress sender_addr;
    bool is_find = false;
    for (const auto & it : m_packages)
    {
        if (it.first == to)
        {
            is_find = true;
            sender_addr = it.second;
            m_packages.removeAll(it);
            break;
        }
    }
    if (!is_find) return false;
    QWebSocket * sender_sock = nullptr;
    for (QWebSocket * sock : m_clients)
    {
        if (sock->peerAddress() == sender_addr.first && sock->peerPort() == sender_addr.second)
        {
            sender_sock = sock;
            break;
        }
    }
    if (sender_sock)
    {
        sender_sock->sendBinaryMessage(with.to_raw());
    }
    return true;
}

NetworkPackage SSL_Server::getPackage()
{
    return m_packages.front().first;
}

SSL_Server::~SSL_Server()
{
    m_serversocket->close();
    qDeleteAll(m_clients.begin(), m_clients.end());
}

void SSL_Server::onSslErrors(const QList<QSslError> &errors)
{
    Q_UNUSED(errors);
}

void SSL_Server::onNewConnection()
{
    QWebSocket *cli_socket = m_serversocket->nextPendingConnection();
    qDebug() << "Client connected:" << cli_socket->peerAddress() << cli_socket->peerPort();

    connect(cli_socket, &QWebSocket::textMessageReceived, this, &SSL_Server::processTextMessage);
    connect(cli_socket, &QWebSocket::binaryMessageReceived,
            this, &SSL_Server::processBinaryMessage);
    connect(cli_socket, &QWebSocket::disconnected, this, &SSL_Server::clientDisconnected);
    cli_socket->sendBinaryMessage(NetworkPackage(NetworkPackage::STATUS_HELLO).to_raw());
    m_clients << cli_socket;
}

void SSL_Server::processTextMessage(QString message)
{
    Q_UNUSED(message);
}

void SSL_Server::processBinaryMessage(QByteArray message)
{
    QWebSocket * cli_socket = qobject_cast<QWebSocket*>(sender());
    if (cli_socket)
    {
        bool is_csum_valid = true;
        m_packages.push_back({
                                 NetworkPackage::from_raw(message, &is_csum_valid),
                                 {cli_socket->peerAddress(), cli_socket->peerPort()}
                             });
        if (!is_csum_valid)
        {
            cli_socket->sendBinaryMessage(NetworkPackage(NetworkPackage::STATUS_PACKAGE_WRONG_CSUM,
                                                                         QByteArray()).to_raw());
            m_packages.pop_back();
        }
        else emit packageReceived();
    }
}

void SSL_Server::clientDisconnected()
{
    QWebSocket * cli_socket = qobject_cast<QWebSocket*>(sender());
    if (cli_socket)
    {
        m_clients.removeAll(cli_socket);
        cli_socket->deleteLater();
    }
}

SSL_Client::SSL_Client(QObject *parent) : QObject(parent)
{
    m_socket = new QWebSocket();
    connect(m_socket, &QWebSocket::connected, this, &SSL_Client::onConnected);
    connect(m_socket, QOverload<const QList<QSslError>&>::of(&QWebSocket::sslErrors),
            this, &SSL_Client::onSslErrors);
}

void SSL_Client::conn(QString addr, quint16 port)
{
    m_socket->open(QString("wss://%1:%2").arg(addr).arg(port));
}

void SSL_Client::sendPackage(const NetworkPackage &pkg)
{
    if (m_socket->isValid())
    {
        m_socket->sendBinaryMessage(pkg.to_raw());
    }
}

NetworkPackage SSL_Client::getPackage()
{
    auto first_package = m_packages.first();
    m_packages.pop_front();
    return first_package;
}

bool SSL_Client::getPackage_blocking(NetworkPackage &out_pkg, quint64 timeout_ms)
{
    QTime t_end = QTime::currentTime().addMSecs(timeout_ms);
    while (QTime::currentTime() <= t_end)
    {
        if (m_packages.size() > 0)
        {
            out_pkg = m_packages.first();
            m_packages.pop_front();
            return true;
        }
        QThread::msleep(30);
    }
    return false;
}

void SSL_Client::onConnected()
{
    connect(m_socket, &QWebSocket::binaryMessageReceived,
            this, &SSL_Client::processBinaryMessage);
    connect(m_socket, &QWebSocket::textMessageReceived,
            this, &SSL_Client::processTextMessage);
}

void SSL_Client::processBinaryMessage(QByteArray message)
{
    bool ok = true;
    m_packages.push_back(NetworkPackage::from_raw(message, &ok));
    emit packageReceived();
}

void SSL_Client::processTextMessage(QString message)
{
    Q_UNUSED(message);
}

void SSL_Client::onSslErrors(const QList<QSslError> &errors)
{
    m_socket->ignoreSslErrors();
    Q_UNUSED(errors);
}
