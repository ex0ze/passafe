#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QUdpSocket>
#include <QThread>
#include <QTime>
#include <QFile>
#include <QtWebSockets/QWebSocketServer>
#include <QtWebSockets/QWebSocket>
#include <QSslKey>
#include <QHash>
#include <QNetworkDatagram>

#include "network_package.h"

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)

using NetworkAddress = QPair<QHostAddress, quint16>;

class SSL_Server : public QObject
{
    Q_OBJECT
public:
    SSL_Server(quint16 port, QObject* parent = nullptr);
    bool replyToPackage(const NetworkPackage& to, const NetworkPackage& with);
    NetworkPackage getPackage();
    ~SSL_Server();
private slots:
    void onSslErrors(const QList<QSslError> &errors);
    void onNewConnection();
    void processTextMessage(QString message);
    void processBinaryMessage(QByteArray message);
    void clientDisconnected();
private:
    QWebSocketServer * m_serversocket;
    QList<QWebSocket*> m_clients;
    QList<QPair<NetworkPackage, NetworkAddress>> m_packages;
signals:
    void packageReceived();
};

class SSL_Client : public QObject
{
    Q_OBJECT
public:
    SSL_Client(QObject* parent = nullptr);
    void conn(QString addr, quint16 port);
    void sendPackage(const NetworkPackage& pkg);
    NetworkPackage getPackage();
    bool getPackage_blocking(NetworkPackage& out_pkg, quint64 timeout_ms);
private slots:
    void onConnected();
    void processBinaryMessage(QByteArray message);
    void processTextMessage(QString message);
    void onSslErrors(const QList<QSslError> &errors);
private:
    QWebSocket * m_socket;
    QList<NetworkPackage> m_packages;
signals:
    void packageReceived();
};


#endif // NETWORK_H
