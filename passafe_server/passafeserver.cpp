#include "passafeserver.h"

PassafeServer::PassafeServer(QObject *parent) : QObject(parent)
{
    m_settings = new QSettings("server_config", QSettings::Format::IniFormat);
    quint16 port = m_settings->value("listening_port", 25910).toInt();
    m_database = new Passafe_database;
    m_server = new SSL_Server(port, this);
    connect(m_server, &SSL_Server::packageReceived, this, &PassafeServer::handle_package);
}

PassafeServer::~PassafeServer()
{
    delete m_database;
}

void PassafeServer::handle_package()
{
    auto pkg = m_server->getPackage();
    switch (pkg.m_context) {
    case NetworkPackage::Context::USER_AUTH: {
        auto creds = UserAuth::from_raw(pkg.m_data);
        Passafe_database::Passafe_auth_status st;
        st = m_database->check_auth(creds.m_login, creds.m_password);
        switch (st) {
        case Passafe_database::STATUS_OK:
            if (!m_authorized.contains(creds.m_login)) m_authorized << creds.m_login;
            m_server->replyToPackage(pkg, NetworkPackage(NetworkPackage::Context::STATUS_AUTH_OK));
            break;
        case Passafe_database::STATUS_WRONG_PASS:
            m_server->replyToPackage(pkg, NetworkPackage(NetworkPackage::Context::STATUS_AUTH_FAILED));
            break;
        case Passafe_database::STATUS_USER_DOES_NOT_EXIST:
            m_server->replyToPackage(pkg, NetworkPackage(NetworkPackage::Context::STATUS_AUTH_DOES_NOT_EXIST));
            break;
        }
        break;
    }
    case NetworkPackage::Context::USER_REGISTER: {
        auto creds = UserAuth::from_raw(pkg.m_data);
        bool ok = m_database->register_user(creds.m_login, creds.m_password);
        if (ok) if (!m_authorized.contains(creds.m_login)) m_authorized << creds.m_login;
        m_server->replyToPackage(pkg, NetworkPackage(ok ?
                                                         NetworkPackage::Context::STATUS_REGISTER_OK : NetworkPackage::Context::STATUS_REGISTER_FAILED));
        break;
    }
    case NetworkPackage::Context::DB_SYNC_REQUEST: {
        QString user = pkg.m_data;
        if (!m_authorized.contains(user))
        {
            m_server->replyToPackage(pkg, NetworkPackage(NetworkPackage::Context::STATUS_ACCESS_DENIED));
        }
        else
        {
            m_server->replyToPackage(pkg, NetworkPackage(NetworkPackage::Context::STATUS_SYNC_OK,
                                                         tbl_storage_entry::entrylist_to_raw(m_database->get_user_storage(user))));
        }
        break;
    }
    case NetworkPackage::Context::DB_ENTRY_ADD:
    case NetworkPackage::Context::DB_ENTRY_EDIT:
    case NetworkPackage::Context::DB_ENTRY_DELETE:
    {
        QString user;
        auto e_package = EntryPackage::from_raw(pkg.m_data, &user);
        if (!m_authorized.contains(user))
        {
            m_server->replyToPackage(pkg, NetworkPackage(NetworkPackage::Context::STATUS_ACCESS_DENIED));
        }
        else {
            bool status = m_database->process(e_package);
            m_server->replyToPackage(pkg, NetworkPackage(status ? NetworkPackage::Context::STATUS_ENTRY_OK : NetworkPackage::Context::STATUS_ENTRY_FAILED));
        }
        break;
    }
    }
}
