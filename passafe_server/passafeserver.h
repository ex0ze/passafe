#ifndef PASSAFESERVER_H
#define PASSAFESERVER_H

#include <QObject>
#include <QSettings>
#include <../network/network.h>
#include <../passafe_database/passafe_database.h>

class PassafeServer : public QObject
{
    Q_OBJECT
public:
    explicit PassafeServer(QObject *parent = nullptr);
    ~PassafeServer();
signals:
private:
    SSL_Server * m_server;
    Passafe_database * m_database;
    QSettings * m_settings;
    QSet<QString> m_authorized;
private slots:
    void handle_package();
};

#endif // PASSAFESERVER_H
