QT -= gui
QT += sql network websockets
CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        passafeserver.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    passafeserver.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../network/release/ -lnetwork
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../network/debug/ -lnetwork
else:unix: LIBS += -L$$OUT_PWD/../network/ -lnetwork

INCLUDEPATH += $$PWD/../network
DEPENDPATH += $$PWD/../network

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../network/release/libnetwork.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../network/debug/libnetwork.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../network/release/network.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../network/debug/network.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../network/libnetwork.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../passafe_database/release/ -lpassafe_database
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../passafe_database/debug/ -lpassafe_database
else:unix: LIBS += -L$$OUT_PWD/../passafe_database/ -lpassafe_database

INCLUDEPATH += $$PWD/../passafe_database
DEPENDPATH += $$PWD/../passafe_database

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../passafe_database/release/libpassafe_database.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../passafe_database/debug/libpassafe_database.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../passafe_database/release/passafe_database.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../passafe_database/debug/passafe_database.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../passafe_database/libpassafe_database.a
