#include <QCoreApplication>
#include "passafeserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    PassafeServer srv;

    return a.exec();
}
