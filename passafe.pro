TEMPLATE = subdirs

SUBDIRS += \
    network \
    passafe_client \
    passafe_database \
    passafe_server
